package org.arvydas.crawler;

import org.arvydas.crawler.wal.api.jobs.WalApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "walApi", url = "${walUrl}")
public interface FeignWalApi extends WalApi {
}
