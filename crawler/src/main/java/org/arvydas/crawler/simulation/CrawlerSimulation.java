package org.arvydas.crawler.simulation;

import com.google.common.collect.Iterators;
import org.arvydas.crawler.simulation.scenarios.Scenario;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Set;

@Service
public class CrawlerSimulation {

    private final Iterator<Scenario> scenarios;

    public CrawlerSimulation(Set<Scenario> scenarios) {
        this.scenarios = Iterators.cycle(scenarios);
    }

    @Scheduled(fixedDelay = 1000)
    public void start() {
        scenarios.forEachRemaining(Scenario::run);
    }
}
