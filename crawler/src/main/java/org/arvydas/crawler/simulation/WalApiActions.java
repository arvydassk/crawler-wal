package org.arvydas.crawler.simulation;

import org.arvydas.crawler.wal.api.jobs.AddRepositoryRequest;
import org.arvydas.crawler.wal.api.jobs.JobRegistration;
import org.arvydas.crawler.wal.api.jobs.WalApi;
import org.arvydas.crawler.wal.api.jobs.people.AddPeopleRequest;
import org.arvydas.crawler.wal.api.jobs.people.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.Callable;

import static com.google.common.collect.Sets.newHashSet;

@Service
public class WalApiActions {

    private static final Logger log = LoggerFactory.getLogger(WalApiActions.class);

    private final WalApi walController;

    public WalApiActions(WalApi walController) {
        this.walController = walController;
    }

    public Long registerJob(String siteUrl) {
        log.info("Registering job with site: {}", siteUrl);
        return walController.registerJob(new JobRegistration(siteUrl)).getJobId();
    }

    public void addRepositoryKey(Long jobId) {
        log.info("Setting repository key to job with id: {}", jobId);
        AddRepositoryRequest addRepositoryRequest = new AddRepositoryRequest( "someKey");
        wrapWithExceptionHandler(() -> walController.addRepositoryKey(jobId, addRepositoryRequest));
    }

    public void addPeople(Long jobId) {
        log.info("Adding people to job with id: {}", jobId);

        Person john = new Person("John", "Smith");
        Person ana = new Person("Ana", "Smith");
        AddPeopleRequest peopleRequest = new AddPeopleRequest(newHashSet(john, ana));
        
        wrapWithExceptionHandler(() -> walController.addPeople(jobId, peopleRequest));
    }

    public void getJobByUrl(Long jobId) {
        log.info("Retrieving info about job with id: {}", jobId);
        wrapWithExceptionHandler(() -> walController.getJobById(jobId));
    }

    public void getUnfinishedJobs() {
        log.info("Retrieving unfinished jobs");
        walController.findUnfinishedJobs(10, 10);
    }
    
    private static void wrapWithExceptionHandler(Callable walApiCall) {
        try {
            walApiCall.call();
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        
    }
}
