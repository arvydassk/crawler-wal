package org.arvydas.crawler.simulation.scenarios;

public interface Scenario {

    void run();
}
