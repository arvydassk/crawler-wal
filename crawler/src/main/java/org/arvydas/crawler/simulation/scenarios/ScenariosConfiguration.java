package org.arvydas.crawler.simulation.scenarios;

import org.arvydas.crawler.simulation.WalApiActions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
class ScenariosConfiguration {

    private final SiteUrlGenerator urlGenerator;
    private final WalApiActions walApiActions;

    ScenariosConfiguration(SiteUrlGenerator urlGenerator, WalApiActions walApiActions) {
        this.urlGenerator = urlGenerator;
        this.walApiActions = walApiActions;
    }

    @Bean
    Scenario happyPathScenario() {
        return () -> {
            String siteUrl = urlGenerator.generate(1000);
            Long jobId = walApiActions.registerJob(siteUrl);
            walApiActions.addPeople(jobId);
            walApiActions.addRepositoryKey(jobId);
        };
    }

    @Bean
    Scenario onlyJobRegistrationScenario() {
        return () -> {
            String siteUrl = urlGenerator.generate(100);
            walApiActions.registerJob(siteUrl);
        };
    }

    @Bean
    Scenario retrievingJobsBySiteUrlScenario() {
        return () -> {
            int nextInt = new Random().nextInt(100);
            walApiActions.getJobByUrl(new Long(nextInt));
        };
    }

    @Bean
    Scenario retrievingUnfinishedJobsScenario() {
        return () -> walApiActions.getUnfinishedJobs();
    }

    @Bean
    Scenario addPeopleRiskyScenario() {
        return () -> {
            int nextInt = new Random().nextInt(100);
            walApiActions.addPeople(new Long(nextInt));
        };
    }
    
    @Bean
    Scenario addRepositoryKeyRiskyScenario() {
        return () -> {
            int nextInt = new Random().nextInt(100);
            walApiActions.addRepositoryKey(new Long(nextInt));
        };
    }

}
