package org.arvydas.crawler.simulation.scenarios;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
class SiteUrlGenerator {

    private final String siteUrlTemplate;

    SiteUrlGenerator(@Value("${simulation.url.template:http://www.google.com}") String siteUrlTemplate) {
        this.siteUrlTemplate = siteUrlTemplate;
    }

    String generate(int numberOfVariations) {
        int randomSuffix = new Random().nextInt(numberOfVariations);
        return String.format("%s/%s", siteUrlTemplate, randomSuffix);
    }
}
