package org.arvydas.crawler.wal.api.jobs;

import org.arvydas.crawler.wal.api.jobs.people.AddPeopleRequest;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RequestMapping(value = "/api/jobs", produces = APPLICATION_JSON_VALUE)
public interface WalApi {

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    JobRegistered registerJob(@Valid @RequestBody JobRegistration registration);

    @PostMapping(path = "{jobId}/people", consumes = APPLICATION_JSON_VALUE)
    PeopleAdded addPeople(@PathVariable("jobId")Long jobId, @Valid @RequestBody AddPeopleRequest request);

    @PostMapping(path = "{jobId}/repository-key", consumes = APPLICATION_JSON_VALUE)
    RepositoryKeyAdded addRepositoryKey(@PathVariable("jobId") Long jobId, @Valid @RequestBody AddRepositoryRequest request);

    @GetMapping(path = "{jobId}")
    JobRepresentation getJobById(@PathVariable("jobId") Long jobId);

    @GetMapping("/unfinished")
    UnfinishedJobs findUnfinishedJobs(@Min(0) @Max(100) @RequestParam("page") int page,
                                      @Min(0) @Max(100) @RequestParam("size") int size);
}
