package org.arvydas.crawler.wal.api.jobs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

public class AddRepositoryRequest {

    @NotBlank
    private String repositoryKey;

    @JsonCreator
    public AddRepositoryRequest(@JsonProperty("repositoryKey") String repositoryKey) {
        this.repositoryKey = repositoryKey;
    }

    public String getRepositoryKey() {
        return repositoryKey;
    }

    public void setRepositoryKey(String repositoryKey) {
        this.repositoryKey = repositoryKey;
    }
}
