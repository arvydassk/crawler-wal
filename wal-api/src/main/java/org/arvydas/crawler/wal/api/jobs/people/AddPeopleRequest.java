package org.arvydas.crawler.wal.api.jobs.people;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

public class AddPeopleRequest {

    @Valid
    @NotEmpty
    Set<Person> people = new HashSet<>();

    @JsonCreator
    public AddPeopleRequest(@JsonProperty("people") Set<Person> people) {
        this.people = people;
    }

    public Set<Person> getPeople() {
        return people;
    }

    public void setPeople(Set<Person> people) {
        this.people = people;
    }

}
