package org.arvydas.crawler.wal.api.jobs.people;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotBlank;

import static java.lang.String.format;

public class Person {

    @NotBlank
    private String firstName;
    @NotBlank
    private String secondName;

    @JsonCreator
    public Person(@JsonProperty("firstName") String firstName, @JsonProperty("secondName") String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public String toString() {
        return format(
                "Person {firstName: %s, secondName: %s}",
                firstName,
                secondName
        );
    }
}
