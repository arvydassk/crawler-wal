package org.arvydas.crawler.wal.api.jobs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class JobRegistration extends SiteUrlAwareRequest {

    @JsonCreator
    public JobRegistration(@JsonProperty("siteUrl") String siteUrl) {
        super(siteUrl);
    }
}
