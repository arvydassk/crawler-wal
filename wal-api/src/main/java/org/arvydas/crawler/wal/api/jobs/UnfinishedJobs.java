package org.arvydas.crawler.wal.api.jobs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class UnfinishedJobs {
    
    private final List<String> siteUrls;

    @JsonCreator
    public UnfinishedJobs(@JsonProperty("siteUrls") List<String> siteUrls) {
        this.siteUrls = siteUrls;
    }

    public List<String> getSiteUrls() {
        return siteUrls;
    }
}
