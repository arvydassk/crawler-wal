package org.arvydas.crawler.wal.api.jobs;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

public class JobRegistered extends ResourceSupport {
    
    private final Long jobId;
    private final String warningMessage;

    @JsonCreator
    public JobRegistered(@JsonProperty("jobId")Long jobId,
                         @JsonProperty("warningMessage") String warningMessage) {
        this.jobId = jobId;
        this.warningMessage = warningMessage;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public Long getJobId() {
        return jobId;
    }
}
