package org.arvydas.crawler.wal.api.jobs;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;

public class SiteUrlAwareRequest {

    @URL
    @NotNull
    private String siteUrl;

    public SiteUrlAwareRequest(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getSiteUrl() {
        return siteUrl;
    }
}
