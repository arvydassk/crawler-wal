package org.arvydas.crawler.wal.api.jobs;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.arvydas.crawler.wal.api.jobs.people.Person;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class JobRepresentation  extends ResourceSupport{
    
    private final String siteUrl;
    private final List<Person> people;
    private final String repositoryKey;

    @JsonCreator
    public JobRepresentation(@JsonProperty("siteUrl") String siteUrl,
                             @JsonProperty("people") List<Person> people,
                             @JsonProperty("repositoryKey") String repositoryKey) {
        this.siteUrl = siteUrl;
        this.people = people;
        this.repositoryKey = repositoryKey;
    }

    public String getSiteUrl() {
        return siteUrl;
    }


    public List<Person> getPeople() {
        return people;
    }

    public String getRepositoryKey() {
        return repositoryKey;
    }
}
