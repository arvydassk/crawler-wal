WAL
------

Write ahead log (WAL) stores data (urls, famous people and repository keys) supplied by crawlers.
WAL Exposes Rest API for communication

### Preconditions

* JDK 8
* docker (tested on 17.06.0-ce)
* docker-compose (tested on 17.06.0-ce)

### How to run:

* ./gradlew build	 *builds applications* 
* ./docker-compose *runs wal and 5 crawler applications* 

### Not Implemented

* Security
* Full HATEOAS
* Better test coverage
* Code quality checking plugins (Checkstyle, Codenarc)
* Review libraries used (example: spring boot + hibernate JPA seams to be to heavy)
to achieve lighter faster application
* Pagination (unfinished jobs request)
* Load tests