package org.arvydas.crawler.wal.jobs.exceptions;

public class SiteNotRegisteredException extends RuntimeException {
    
    public SiteNotRegisteredException(String message) {
        super(message);
    }
}
