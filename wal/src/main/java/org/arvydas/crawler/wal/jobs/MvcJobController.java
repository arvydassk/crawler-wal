package org.arvydas.crawler.wal.jobs;

import eu.hinsch.spring.boot.actuator.metric.ExecutionMetric;
import org.arvydas.crawler.wal.api.jobs.*;
import org.arvydas.crawler.wal.api.jobs.people.AddPeopleRequest;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import static org.springframework.boot.logging.LogLevel.INFO;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpStatus.CREATED;

@Validated
@RestController
public class MvcJobController implements WalApi {

    private final JobManager jobManager;

    public MvcJobController(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    @Override
    @ResponseStatus(CREATED)
    @ExecutionMetric(value = "registerJob", loglevel = INFO)
    public JobRegistered registerJob(@Valid @RequestBody JobRegistration registration) {
        JobRegistered jobRegistered = jobManager.register(registration);
        jobRegistered.add(linkTo(methodOn(MvcJobController.class)
                .getJobById(jobRegistered.getJobId()))
                .withSelfRel());
        jobRegistered.add(linkTo(methodOn(MvcJobController.class)
                .addPeople(jobRegistered.getJobId(),null))
                .withRel("addPeople"));
        
        return jobRegistered;
    }

    @Override
    @ResponseStatus(CREATED)
    @ExecutionMetric(value = "addPeople", loglevel = INFO)
    public PeopleAdded addPeople(@PathVariable("jobId") Long jobId, @Valid @RequestBody AddPeopleRequest request) {
        jobManager.addPeople(jobId, request);
        PeopleAdded peopleAdded = new PeopleAdded();
        peopleAdded.add(linkTo(methodOn(MvcJobController.class)
                .addPeople(jobId, null))
                .withSelfRel());
        peopleAdded.add(linkTo(methodOn(MvcJobController.class)
                .addRepositoryKey(jobId, null))
                .withRel("addRepositoryKey"));
        peopleAdded.add(linkTo(methodOn(MvcJobController.class)
                .getJobById(jobId))
                .withRel("getJob"));
        return peopleAdded;
    }

    @Override
    @ExecutionMetric(value = "addRepositoryKey", loglevel = INFO)
    public RepositoryKeyAdded addRepositoryKey(@PathVariable("jobId") Long jobId, @Valid @RequestBody AddRepositoryRequest request) {
        jobManager.setRepositoryKey(jobId, request);
        RepositoryKeyAdded keyAdded = new RepositoryKeyAdded();
        keyAdded.add(linkTo(methodOn(MvcJobController.class)
                .getJobById(jobId))
                .withRel("getJob"));
        return keyAdded;
    }

    @Override
    @ExecutionMetric(value = "getJobById", loglevel = INFO)
    public JobRepresentation getJobById(@PathVariable("jobId") Long jobId) {
        return jobManager.getJob(jobId);
    }

    @Override
    @ExecutionMetric(value = "findUnfinishedJobs", loglevel = INFO)
    public UnfinishedJobs findUnfinishedJobs(@RequestParam("page") @Min(0) @Max(100) int page,
                                             @RequestParam("size") @Min(0) @Max(100) int size) {
        return jobManager.findUnfinished(new PageRequest(page, size));
    }
}
