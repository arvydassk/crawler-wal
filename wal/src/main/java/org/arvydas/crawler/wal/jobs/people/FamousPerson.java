package org.arvydas.crawler.wal.jobs.people;

import org.arvydas.crawler.wal.common.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "famous_person")
public class FamousPerson extends BaseEntity {

    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "last_name")
    private String secondName;

    private FamousPerson() {
    }

    public FamousPerson(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }
}
