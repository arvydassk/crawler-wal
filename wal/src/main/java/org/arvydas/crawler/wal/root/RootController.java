package org.arvydas.crawler.wal.root;

import org.arvydas.crawler.wal.jobs.MvcJobController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/api", produces = APPLICATION_JSON_VALUE)
public class RootController {
    
    @GetMapping
    public Root get() {
        Root root = new Root();
        root.add(linkTo(methodOn(MvcJobController.class)
                .registerJob(null)).withRel("registerJob"));
        root.add(linkTo(methodOn(MvcJobController.class)
                .getJobById(null)).withRel("findJob"));
        root.add(linkTo(methodOn(MvcJobController.class)
                .findUnfinishedJobs(0, 10)).withRel("findUnfinishedJobs"));
        return root;
    }
}
