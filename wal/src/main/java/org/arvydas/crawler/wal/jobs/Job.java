package org.arvydas.crawler.wal.jobs;

import org.arvydas.crawler.wal.common.BaseEntity;
import org.arvydas.crawler.wal.jobs.people.FamousPerson;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "job")
public class Job extends BaseEntity {

    @Column(name = "site_url", nullable = false)
    private String siteUrl;
    
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = LAZY)
    private List<FamousPerson> people = new ArrayList<>();

    @Column(name = "repository_key")
    private String repositoryKey;

    @Column(name = "finished")
    private boolean finished;
    
    private Job() {
    }

    public Job(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public List<FamousPerson> getPeople() {
        return people;
    }

    public void addPerson(FamousPerson person) {
       people.add(person);
        markAsFinished();
    }

    public String getRepositoryKey() {
        return repositoryKey;
    }

    public void setRepositoryKey(String repositoryKey) {
        this.repositoryKey = repositoryKey;
        markAsFinished();
    }

    public boolean isFinished() {
        return finished;
    }

    private void markAsFinished() {
        finished = true;
    }
}
