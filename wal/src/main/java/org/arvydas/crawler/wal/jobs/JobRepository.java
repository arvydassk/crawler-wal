package org.arvydas.crawler.wal.jobs;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface JobRepository extends JpaRepository<Job, Long> {

    Optional<Job> findBySiteUrl(String url);
    List<Job> findByFinishedFalse(Pageable pageable);
    
}
