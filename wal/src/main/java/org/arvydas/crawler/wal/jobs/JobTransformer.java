package org.arvydas.crawler.wal.jobs;

import org.arvydas.crawler.wal.api.jobs.JobRepresentation;
import org.arvydas.crawler.wal.jobs.people.PersonTransformer;

public class JobTransformer {
    
    public static JobRepresentation transform(Job job) {
        return new JobRepresentation(
                job.getSiteUrl(),
                PersonTransformer.transformToRepresentation(job.getPeople()),
                job.getRepositoryKey()
        );
    }
   
}
