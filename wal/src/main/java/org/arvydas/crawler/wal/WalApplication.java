package org.arvydas.crawler.wal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.hateoas.config.EnableHypermediaSupport;

import static org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType.HAL;

@SpringBootApplication
@EnableHypermediaSupport(type= HAL)
public class WalApplication {

    public static void main(String[] args) {
        SpringApplication.run(WalApplication.class, args);
    }
}
