package org.arvydas.crawler.wal.jobs;

import org.arvydas.crawler.wal.api.jobs.*;
import org.arvydas.crawler.wal.api.jobs.people.AddPeopleRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.arvydas.crawler.wal.jobs.people.PersonTransformer.transformToEntity;

@Service
public class JobManager {
    
    private static final Logger log = LoggerFactory.getLogger(JobManager.class);

    private final JobRepository jobRepository;
    private final JobProvider jobProvider;

    public JobManager(JobRepository jobRepository, JobProvider jobProvider) {
        this.jobRepository = jobRepository;
        this.jobProvider = jobProvider;
    }

    @Transactional
    public JobRegistered register(JobRegistration request) {
        String url = request.getSiteUrl();
        Optional<Job> job = jobRepository.findBySiteUrl(url);

        if (job.isPresent()) {
            log.info("Waring: Job with site {} already registered. Will skip registration", url);
            return new JobRegistered(job.get().getId(), "Site already registered!");
        } else {
            Job savedJob = jobRepository.save(new Job(url));
            log.info("Registered job with site {}", url);
            return new JobRegistered(savedJob.getId(), null);
        }
    }

    @Transactional
    public void addPeople(Long jobId, AddPeopleRequest request) {
        Job job = jobProvider.getRequired(jobId);
        transformToEntity(request.getPeople()).forEach(job::addPerson);
        log.info("Added famous people {} for job with site {}", request.getPeople(), jobId);
    }

    @Transactional
    public void setRepositoryKey(Long jobId, AddRepositoryRequest request) {
        Job site = jobProvider.getRequired(jobId);
        site.setRepositoryKey(request.getRepositoryKey());
        log.info("Set repository key {} for job with id {}", request.getRepositoryKey(), jobId);
    }

    @Transactional(readOnly = true)
    public JobRepresentation getJob(Long jobId) {
        Job job = jobProvider.getRequired(jobId);
        return JobTransformer.transform(job);
    }

    @Transactional(readOnly = true)
    public UnfinishedJobs findUnfinished(Pageable pageable) {
        List<String> unfinishedUrls = jobRepository.findByFinishedFalse(pageable).stream()
                .map(j -> j.getSiteUrl())
                .collect(toList());
        return new UnfinishedJobs(unfinishedUrls);
    }
}
