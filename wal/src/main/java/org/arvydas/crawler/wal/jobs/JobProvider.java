package org.arvydas.crawler.wal.jobs;

import org.arvydas.crawler.wal.jobs.exceptions.SiteNotRegisteredException;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

@Service
public class JobProvider {

    private final JobRepository siteRepository;

    public JobProvider(JobRepository jobRepository) {
        this.siteRepository = jobRepository;
    }

    public Job getRequired(Long id) {
        return getOptional(id)
                .orElseThrow(
                () -> new SiteNotRegisteredException(format("Job with id '%s' is missing!", id))
        );
    }

    public Optional<Job> getOptional(Long id) {
        requireNonNull(id, "Job id must not be null");
        return ofNullable(siteRepository.findOne(id));
    }
}
