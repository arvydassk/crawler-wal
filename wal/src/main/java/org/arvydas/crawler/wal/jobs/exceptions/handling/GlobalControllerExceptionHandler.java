package org.arvydas.crawler.wal.jobs.exceptions.handling;

import org.arvydas.crawler.wal.api.ErrorResponse;
import org.arvydas.crawler.wal.jobs.exceptions.SiteNotRegisteredException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(SiteNotRegisteredException.class)
    public ResponseEntity<ErrorResponse> handleSiteNotFound(SiteNotRegisteredException exception) {
        return ResponseEntity.status(BAD_REQUEST).body(new ErrorResponse(exception.getMessage()));
    }
}
