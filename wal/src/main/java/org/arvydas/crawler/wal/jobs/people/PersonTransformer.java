package org.arvydas.crawler.wal.jobs.people;

import org.arvydas.crawler.wal.api.jobs.people.Person;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PersonTransformer {

    public static List<Person> transformToRepresentation(List<FamousPerson> people) {
        return people.stream()
                .map(PersonTransformer::transform)
                .collect(Collectors.toList());
    }

    public static List<FamousPerson> transformToEntity(Set<Person> people) {
        return people.stream()
                .map(PersonTransformer::transform)
                .collect(Collectors.toList());
    }

    private static FamousPerson transform(Person person) {
        return new FamousPerson(person.getFirstName(), person.getSecondName());
    }

    private static Person transform(FamousPerson famousPerson) {
        return  new Person(famousPerson.getFirstName(), famousPerson.getSecondName());
    }
}
