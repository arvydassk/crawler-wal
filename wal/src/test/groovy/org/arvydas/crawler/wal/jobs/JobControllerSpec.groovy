package org.arvydas.crawler.wal.jobs

import org.arvydas.crawler.wal.AbstractApiSpecification
import org.springframework.hateoas.Link

import static org.springframework.http.HttpStatus.*

class JobControllerSpec extends AbstractApiSpecification {

    def 'should register job'() {
        when:
            def siteUrl = 'http://www.google.lt'
            def entity = apiClient.register(siteUrl)
        then:
            entity.statusCode == CREATED
            def response = entity.body
            !response.warningMessage
            response.getLink("self")
            response.getLink("addPeople")
        and:
            assertSiteAdded(response.getLink('self'), siteUrl)
    }

    def 'should return warning message if job already exists'() {
        when:
            def siteUrl = 'http://www.google.lt/1'
            apiClient.register(siteUrl)
        then:
            def entity = apiClient.register(siteUrl)
        expect:
            entity.statusCode == CREATED
            entity.body.warningMessage == 'Site already registered!'
    }

    def 'should add famous people to job record'() {
        when:
            def siteUrl = 'http://www.google.lt/2'
            def addPeopleLink = apiClient.register(siteUrl).body.getLink('addPeople')
        then:
            def entity = apiClient.addPeople(addPeopleLink)
        expect:
            entity.statusCode == CREATED
            def response = entity.body
            response.getLink('self')
            response.getLink('addRepositoryKey')
        and:
            assertPeopleAdded(apiClient.getJobUrl(response))
    }

    def 'should add remote key to job'() {
        when:
            def siteUrl = 'http://www.google.lt/3'
            def addPeopleLink = apiClient.register(siteUrl).body.getLink('addPeople')
            def addRepositoryKeyLink = apiClient.addPeople(addPeopleLink).body.getLink('addRepositoryKey')
        then:
            def entity = apiClient.addRepositoryKey(addRepositoryKeyLink)
        expect:
            entity.statusCode == OK
            def response = entity.body
            !response.getLink('addRepositoryKey')
            !response.getLink('addPeople')
        and:
            assertRepositoryKeySet(apiClient.getJobUrl(response))
    }

    def 'should fail to add people if job is not registered'() {
        when:
            def entity = apiClient.addPeople(new Link('/api/jobs/10/people'))
        then:
            entity.statusCode == BAD_REQUEST
    }

    def 'should fail to add repository key to not registered job'() {
        when:
            def entity = apiClient.addPeople(new Link('/api/jobs/10/repository-key'))
        then:
            entity.statusCode == BAD_REQUEST
    }

    def 'should receive multiple unfinished jobs'() {
        when:
            apiClient.register('http://www.google.lt/7')
            apiClient.register('http://www.google.lt/8')
        then:
            def entity = apiClient.findUnfinishedJobs(0, 10)
        expect:
            entity.statusCode == OK
            def urls = entity.body.siteUrls
            urls.contains('http://www.google.lt/7')
            urls.contains('http://www.google.lt/8')
    }

    private void assertSiteAdded(Link jobLink, String siteUrl) {
        def entity = apiClient.getJob(jobLink)
        with(entity) {
            entity.statusCode == OK
            entity.body.siteUrl == siteUrl
        }
    }

    private void assertPeopleAdded(Link jobLink) {
        def entity = apiClient.getJob(jobLink)
        with(entity) {
            entity.statusCode == OK
            entity.body.people.any { it.firstName == 'John' && it.secondName == 'Smith' }
        }
    }

    private void assertRepositoryKeySet(Link jobLink) {
        def entity = apiClient.getJob(jobLink)
        with(entity) {
            entity.statusCode == OK
            entity.body.repositoryKey == 'some-key'
        }
    }

}
