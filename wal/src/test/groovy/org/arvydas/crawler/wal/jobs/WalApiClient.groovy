package org.arvydas.crawler.wal.jobs

import org.arvydas.crawler.wal.api.jobs.*
import org.arvydas.crawler.wal.api.jobs.people.AddPeopleRequest
import org.arvydas.crawler.wal.api.jobs.people.Person
import org.arvydas.crawler.wal.root.Root
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.hateoas.Link
import org.springframework.hateoas.ResourceSupport
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component

@Component
class WalApiClient {

    private final TestRestTemplate restTemplate

    WalApiClient(TestRestTemplate restTemplate) {
        this.restTemplate = restTemplate
    }

     ResponseEntity<JobRegistered> register(String siteUrl) {
        restTemplate.postForEntity(
                '/api/jobs',
                new JobRegistration(siteUrl),
                JobRegistered
        )
    }

     Link getJobUrl(ResourceSupport response) {
        response.getLink('getJob')
    }

     ResponseEntity<PeopleAdded> addPeople(Link addPeopleLink) {
        def john = new Person('John', 'Smith')
        def addPeopleRequest = new AddPeopleRequest( [john] as Set)

        restTemplate.postForEntity(
                addPeopleLink.href,
                addPeopleRequest,
                PeopleAdded
        )
    }

     ResponseEntity<PeopleAdded> addRepositoryKey(Link addRepositoryKeyLink) {
        def repositoryRequest = new AddRepositoryRequest( 'some-key')

        restTemplate.postForEntity(
                addRepositoryKeyLink.href,
                repositoryRequest,
                RepositoryKeyAdded
        )
    }

     ResponseEntity<JobRepresentation> getJob(Link jobLink) {
        restTemplate.getForEntity(jobLink.href, JobRepresentation)
    }

     ResponseEntity<UnfinishedJobs> findUnfinishedJobs(int page, int size) {
        restTemplate.getForEntity(
                '/api/jobs/unfinished?page={page}&size={size}',
                UnfinishedJobs,
                page,
                size
        )
    }
    
    ResponseEntity<Root> getRoot() {
        restTemplate.getForEntity('/api', Root)
    }
}
