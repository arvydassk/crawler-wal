package org.arvydas.crawler.wal.jobs

import org.arvydas.crawler.wal.api.jobs.AddRepositoryRequest
import org.arvydas.crawler.wal.api.jobs.JobRegistration
import org.arvydas.crawler.wal.api.jobs.people.AddPeopleRequest
import org.arvydas.crawler.wal.api.jobs.people.Person
import org.arvydas.crawler.wal.jobs.people.FamousPerson
import spock.lang.Specification

import static org.arvydas.crawler.wal.jobs.JobManagerSpec.FamousPersonAssertion.assertPerson

class JobManagerSpec extends Specification {

    static final long JOB_ID = 1L
    static final String SITE_URL = 'http://google.lt'

    JobRepository repository = Mock()
    JobProvider provider = Stub()
    JobManager jobManager = new JobManager(repository, provider)

    void setup() {
    }

    def 'should register new job without a warning'() {
        given:
            noSameJobsWereRegistered()
        when:
            def jobRegistered = jobManager.register(new JobRegistration(SITE_URL))
        then:
            1 * repository.save(_ as Job) >> { Job job ->
                job.id = JOB_ID
                job
            }
            !jobRegistered.warningMessage
            jobRegistered.jobId == 1
    }

    def 'should return warning on registration if job with same url was already saved'() {
        given:
            sameJobWasAlreadyRegistered()
        when:
            def jobRegistered = jobManager.register(new JobRegistration(SITE_URL))
        then:
            jobRegistered.jobId == 1
            jobRegistered.warningMessage == 'Site already registered!'
    }

    def 'should add people to existing job'() {
        given:
            def job = new Job(SITE_URL)
            provider.getRequired(1L) >> job
        and:
            def addPeopleRequest = new AddPeopleRequest([
                    new Person('John', 'Smith'),
                    new Person('Ana', 'Miller')
            ] as Set)
        when:
            jobManager.addPeople(JOB_ID, addPeopleRequest)
        then:
            job.finished
            job.people.size() == 2
            assertPerson(job.people[0]).hasFirstName('John').hasSecondName('Smith')
            assertPerson(job.people[1]).hasFirstName('Ana').hasSecondName('Miller')
    }

    def 'should add repository key'() {
        given:
            def job = new Job(SITE_URL)
            provider.getRequired(1L) >> job
            def addRepositoryRequest = new AddRepositoryRequest('someKey')
        when:
            jobManager.setRepositoryKey(JOB_ID, addRepositoryRequest)
        then:
            job.finished
            job.repositoryKey == 'someKey'
    }

    private void noSameJobsWereRegistered() {
        repository.findBySiteUrl(SITE_URL) >> Optional.empty()
    }

    private void sameJobWasAlreadyRegistered() {
        def job = new Job(SITE_URL)
        job.id = 1
        repository.findBySiteUrl(SITE_URL) >> Optional.of(job)
    }

    static class FamousPersonAssertion {

        private final FamousPerson person

        private FamousPersonAssertion(FamousPerson person) {
            this.person = person
        }

        static FamousPersonAssertion assertPerson(FamousPerson famousPerson) {
            new FamousPersonAssertion(famousPerson)
        }

        FamousPersonAssertion hasFirstName(String fistName) {
            assert person.firstName == fistName
            this
        }

        FamousPersonAssertion hasSecondName(String secondName) {
            assert person.secondName == secondName
            this
        }
    }
}
