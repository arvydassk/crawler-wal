package org.arvydas.crawler.wal

import org.arvydas.crawler.wal.jobs.WalApiClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@SpringBootTest(webEnvironment = RANDOM_PORT) 
abstract class AbstractApiSpecification extends Specification {
    
    @Autowired
    protected WalApiClient apiClient
   
}
