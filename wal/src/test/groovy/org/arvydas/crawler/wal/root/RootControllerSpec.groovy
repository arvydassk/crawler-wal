package org.arvydas.crawler.wal.root

import org.arvydas.crawler.wal.AbstractApiSpecification

import static org.springframework.http.HttpStatus.OK

class RootControllerSpec extends AbstractApiSpecification {
    
    def 'should provide response with links of /api resource'() {
        when:
            def entity = apiClient.root
        then:
            entity.statusCode == OK
            entity.body.getLink('registerJob')
            entity.body.getLink('findJob')
            entity.body.getLink('findUnfinishedJobs')
    }
}
